// global-scope variables (they live as long as the program is running)
let firstFlip = {imageIndex: null, placementValue: null, element: null}; //object to store current flip details
let cleverUtf8Characters = ['&#127753;','&#127756;','&#127755;','&#127754;','&#127747;','&#127752;','&#127776;','&#127789;','&#127798;','&#127812;'];
let lockCharacter = '&#128274;';
let placementArray = [];//we will shuffle the below array randomly
let numberMatched = 0;
let numberFlips = 0;

//Create references to my HTML elements
const mainContentDiv = document.getElementById('main-content');
const mainTitleDiv = document.getElementById('main-title');
const resetButton = document.getElementById('reset');

//Borrowed from Episode 1 "algorithm"
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

//logic to happen upon click of image
function flipImage(imageIndex, element){
    //if placementArray has a null - that means they click on an already flipped image
    if(placementArray[imageIndex] == null){
        //do nothing else after this in flipImage function
        return;
    }
    //increase numberFlips count by 1 (++ is a convenient way of saying add 1 to the current value)
    numberFlips++;
    //if firstFlip is null, we determine this is our first out of two flips
    if(firstFlip.imageIndex == null){
        //update lock image to be whichever image is found in placementArray
        element.innerHTML = cleverUtf8Characters[placementArray[imageIndex]];
        //store details to use in second flip
        firstFlip = {placementValue: placementArray[imageIndex], imageIndex: imageIndex, element: element};
    } else {
        //second flip
        //update lock image to be whichever image is found in placementArray
        element.innerHTML = cleverUtf8Characters[placementArray[imageIndex]];
        //match checking logic
        if (firstFlip.placementValue == placementArray[imageIndex]){
            //found a match
            //set placementArray values for those two matched images to null (so we know they are no longer considered and stay shown)
            placementArray[firstFlip.imageIndex] = null;
            placementArray[imageIndex] = null;
            //clear firstFlip
            firstFlip = {imageIndex: null, placementValue: null, element: null};
            //increase matches found count
            numberMatched ++;
            //see if the game is over
            checkIfGameOver();
        }else {
            //not a match
            //wait 1 second (1000 milliseconds) before setting images back to 'lock' image
            setTimeout(() => {
                firstFlip.element.innerHTML = lockCharacter;
                element.innerHTML = lockCharacter;
                //clear firstFlip
                firstFlip = {imageIndex: null, placementValue: null, element: null};
            }, 1000)    
        }
    }
}

function checkIfGameOver(){
    //check if number matches equals 10
    if(numberMatched == 10){
        //if true, append some text to title element
        mainTitleDiv.innerHTML = "You Won in " + (numberFlips / 2) + " Trys!!"
    }
}

resetButton.addEventListener("click", (e) => {
    //BONUS logic to reset our state without needing to refresh browser page
    mainTitleDiv.innerHTML = "Match 2 Memory Game";
    numberMatched = 0;
    numberFlips = 0;
    //setup new game
    generateGameBoard();
});

function generateGameBoard(){
    //fresh game
    mainContentDiv.innerHTML = '';
    //setup 2 of each unique images
    placementArray = [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9];
    //shuffle their location in array
    shuffleArray(placementArray);

    let newDivElement;
    //create our 4x5 grid of 'images'
    for(let i = 0; i < 20; i++){
        if(i % 4 == 0){
            //modulo operation does this logic only once every 4 loops (since im checking i)
            newDivElement = document.createElement('div');
            newDivElement.id = 'row'+i / 4;
        }
        let newSpanElement = document.createElement('span');
        newSpanElement.style.fontSize = '5em';
        newSpanElement.innerHTML = lockCharacter;
        newSpanElement.id = i;
        newSpanElement.addEventListener("click", function(e) {
            //sets up listenter for user input of an image, flips and evaluates that image
            // e is a reference to the event, and you grab the element from the 'target' variable
            flipImage(parseInt(e.target.id), e.target);
        });
        newDivElement.appendChild(newSpanElement)

        if(i % 4 == 0){
            mainContentDiv.appendChild(newDivElement);
        }
    }
}
//this tells the program to render/setup game when you load page
generateGameBoard();
